package com.siwaak.ms.galleryservice.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class Gallery {

	public Gallery(int galleryId) {
		this.id = galleryId;
	}
	long id;
	List<Object> images;

}
